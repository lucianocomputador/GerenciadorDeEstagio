package percistencia;

import jdk.nashorn.internal.runtime.Source;

/**
 * Classe que contem os nomes dos arquivos XML
 * 
 * @author Luciano Azevedo
 *
 */
public class ArquivosXML {
	
	public static final String ALUNO = "Aluno.XML";
	public static final String INSTITUICAO = "Instituicao.XML";
	public static final String CURSO = "Curso.XML";
	public static final String ESTAGIO = "Estagios.XML";
	public static final String ORIENTADOR = "Orientador.XML";
	public static final String VISITAS = "Visitas.XML";

}

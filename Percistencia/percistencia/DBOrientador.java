package percistencia;

public class DBOrientador extends StrategyDB {

	@Override
	protected void salvar() {
		handler.salvar(ArquivosXML.ORIENTADOR, obj);

	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.ORIENTADOR);
	}

}

package percistencia;

//import persistencia.XML;

/**
 * Classe modelo para salvar e recuperar dados
 * Utilizando padr�o Strategy
 * 
 * @author Luciano Azevedo
 *
 * @param <P> passar tipo de objeto a ser manipulado
 */
public abstract class StrategyDB<P> {
	
	protected XML<P> handler = new XML<P>();
	
	protected Object obj;
	/**
	 * Metodo usado pelas classe que utilizar esta, para salvar dados
	 * @param obj Passar objeto a ser salvo
	 */
	public void salva(Object obj){
		this.obj = obj;
		salvar();
	}
	
	/**
	 * Metodo usado pelas classes que utilizar esta, para recuperar dados
	 * @return Retorna um objeto ao qual dever� se usado 
	 */
	public Object recupera(){
		return recuperar();
	}
	
	//Metodos fablica
	
	/**
	 * Metodo utilizada pelas sub-classes para salvar dados, N�O USAR 
	 * METODO factory
	 */
	protected abstract void salvar();
	
	/**
	 * Metodo utilizada pelas sub-classes para recuperar dados objetos, N�O USAR 
	 * METODO factory
	 */

	protected abstract Object recuperar();
	
	
	

}

package percistencia;

import entidades.Instituicao;

public class DBInstituicao extends StrategyDB<Instituicao>{

	@SuppressWarnings("static-access")
	protected void salvar() {
		handler.salvar(ArquivosXML.INSTITUICAO, obj);
	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.INSTITUICAO);
	}

}

package percistencia;

import entidades.Estagio;

/**
 * Classe estrategica para maipular DB ESTAGIO
 * 
 * @author Luciano Azevedo
 *
 */
public class DBEstagio extends StrategyDB<Estagio> {

	@SuppressWarnings("static-access")
	protected void salvar() {
		handler.salvar(ArquivosXML.ESTAGIO, obj);
	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.ESTAGIO);
	}

}

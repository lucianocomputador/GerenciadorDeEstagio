package percistencia;

import entidades.Curso;

public class DBCurso extends StrategyDB<Curso> {

	@SuppressWarnings("static-access")
	protected void salvar() {
		handler.salvar(ArquivosXML.CURSO, obj);
	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.CURSO);
	}

}
package percistencia;

import entidades.Visita;

public class DBVisita extends StrategyDB<Visita> {

	@SuppressWarnings("static-access")
	protected void salvar() {
		handler.salvar(ArquivosXML.VISITAS, obj);
	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.VISITAS);
	}

}

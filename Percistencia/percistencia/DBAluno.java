package percistencia;

import entidades.Aluno;

/**
 * Classe estrategica para maipular DB de EVENTOS
 * 
 * @author Luciano Azevedo
 *
 */
public class DBAluno extends StrategyDB<Aluno> {

	@SuppressWarnings("static-access")
	protected void salvar() {
		handler.salvar(ArquivosXML.ALUNO, obj);
	}

	protected Object recuperar() {
		return handler.recuperar(ArquivosXML.ALUNO);
	}

}

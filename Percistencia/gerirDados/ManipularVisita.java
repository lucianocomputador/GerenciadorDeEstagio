package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Orientador;
import entidades.Visita;
import percistencia.DBAluno;
import percistencia.DBOrientador;
import percistencia.DBVisita;
import percistencia.StrategyDB;

public class ManipularVisita implements Manipuladores<Visita> {

	private static ManipularVisita instance;

	private StrategyDB<Visita> DBVisita;
	private ArrayList<Visita> lista;

	@Override
	public ArrayList<Visita> listaTodos() {
		return lista;
	}

	@Override
	public void add(Visita elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Visita elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	/*Buscar Visitas realizados pelo orientador ao estagio do aluno.
	 * param id deve ser passado o id do estagio para que o mesmo seja sincronizado com o estagio
	 */
	@Override
	public Visita buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).getEstagio()==id) {
				return lista.get(i);
			}
		}
		return null;

	}
	private void salvar() {
		DBVisita.salva(lista);
	}

	@SuppressWarnings("unchecked")
	private ManipularVisita() {
		DBVisita = new DBVisita();
		lista = (ArrayList<Visita>) DBVisita.recupera();

	}

	public static ManipularVisita getInstace() {
		if (instance == null) {
			synchronized (ManipularVisita.class) {
				if (instance == null) {
					instance = new ManipularVisita();
				}
			}
		}
		return instance;
	}

}

package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Orientador;
import percistencia.DBAluno;
import percistencia.DBOrientador;
import percistencia.StrategyDB;
import validar.Validar;

public class ManipularOrientador implements Manipuladores<Orientador> {

	private static ManipularOrientador instance;

	private StrategyDB<Orientador> DBOrientador;
	private ArrayList<Orientador> lista;

	@Override
	public ArrayList<Orientador> listaTodos() {
		return lista;
	}

	@Override
	public void add(Orientador elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Orientador elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	@Override
	public Orientador buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(Validar.pegarNumeros(lista.get(i).getMatricula())==id) {
				return lista.get(i);
			}
		}
		return null;

	}
	private void salvar() {
		DBOrientador.salva(lista);
	}

	@SuppressWarnings("unchecked")
	private ManipularOrientador() {
		DBOrientador = new DBOrientador();
		lista = (ArrayList<Orientador>) DBOrientador.recupera();

	}

	public static ManipularOrientador getInstace() {
		if (instance == null) {
			synchronized (ManipularOrientador.class) {
				if (instance == null) {
					instance = new ManipularOrientador();
				}
			}
		}
		return instance;
	}

}

package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Curso;
import percistencia.DBAluno;
import percistencia.DBCurso;
import percistencia.StrategyDB;

public class ManipularCurso implements Manipuladores<Curso> {

	private static ManipularCurso instance;

	private StrategyDB<Curso> Dbcurso;
	private ArrayList<Curso> lista;

	@Override
	public ArrayList<Curso> listaTodos() {
		return lista;
	}

	@Override
	public void add(Curso elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Curso elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	@Override
	public Curso buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).getId()==id) {
				return lista.get(i);
			}
		}
		return null;
	}
	private void salvar() {
		Dbcurso.salva(lista);
	}

	@SuppressWarnings("unchecked")
	private ManipularCurso() {
		Dbcurso = new DBCurso();
		lista = (ArrayList<Curso>) Dbcurso.recupera();

	}

	public static ManipularCurso getInstace() {
		if (instance == null) {
			synchronized (ManipularCurso.class) {
				if (instance == null) {
					instance = new ManipularCurso();
				}
			}
		}
		return instance;
	}

}

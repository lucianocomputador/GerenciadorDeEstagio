package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Estagio;
import percistencia.DBAluno;
import percistencia.StrategyDB;
import validar.Validar;

public class ManipularAluno implements Manipuladores<Aluno> {

	private static ManipularAluno instance;

	private StrategyDB<Aluno> DBAlunos;
	private ArrayList<Aluno> lista;

	@Override
	public ArrayList<Aluno> listaTodos() {
		return lista;
	}

	@Override
	public void add(Aluno elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Aluno elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	@Override
	public Aluno buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(Validar.pegarNumeros(lista.get(i).getMatricula())==id) {
				return lista.get(i);
			}
		}
		return null;

	}
	private void salvar() {
		DBAlunos.salva(lista);
	}

	private ManipularAluno() {
		DBAlunos = new DBAluno();
		lista = (ArrayList<Aluno>) DBAlunos.recupera();

	}

	public static ManipularAluno getInstace() {
		if (instance == null) {
			synchronized (ManipularAluno.class) {
				if (instance == null) {
					instance = new ManipularAluno();
				}
			}
		}
		return instance;
	}

}

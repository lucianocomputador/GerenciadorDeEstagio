package gerirDados;

import java.util.ArrayList;


public interface Manipuladores <t>{
	
	public ArrayList<t> listaTodos();
	
	public void add(t elemento);
	
	public void remover(t elemento);
	
	public t buscar(double id);
	

}

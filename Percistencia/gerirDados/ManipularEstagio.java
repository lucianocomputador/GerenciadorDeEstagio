package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Estagio;
import percistencia.DBAluno;
import percistencia.DBEstagio;
import percistencia.StrategyDB;

public class ManipularEstagio implements Manipuladores<Estagio> {

	private static ManipularEstagio instance;

	private StrategyDB<Estagio> DBEstagio;
	private ArrayList<Estagio> lista;

	@Override
	public ArrayList<Estagio> listaTodos() {
		return lista;
	}

	@Override
	public void add(Estagio elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Estagio elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	@Override
	public Estagio buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).getId()==id) {
				return lista.get(i);
			}
		}
		return null;

	}
	private void salvar() {
		DBEstagio.salva(lista);
	}

	private ManipularEstagio() {
		DBEstagio = new DBEstagio();
		lista = (ArrayList<Estagio>) DBEstagio.recupera();

	}

	public static ManipularEstagio getInstace() {
		if (instance == null) {
			synchronized (ManipularEstagio.class) {
				if (instance == null) {
					instance = new ManipularEstagio();
				}
			}
		}
		return instance;
	}

}

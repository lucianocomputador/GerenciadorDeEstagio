package gerirDados;

import java.util.ArrayList;

import entidades.Aluno;
import entidades.Instituicao;
import percistencia.DBAluno;
import percistencia.DBInstituicao;
import percistencia.StrategyDB;
import validar.Validar;

public class ManipularInstituicao implements Manipuladores<Instituicao> {

	private static ManipularInstituicao instance;

	private StrategyDB<Instituicao> DBInstituicao;
	private ArrayList<Instituicao> lista;

	@Override
	public ArrayList<Instituicao> listaTodos() {
		return lista;
	}

	@Override
	public void add(Instituicao elemento) {
		lista.add(elemento);
		salvar();

	}

	@Override
	public void remover(Instituicao elemento) {
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).equals(elemento)) {
				lista.remove(i);
			}
		}
		salvar();
	}

	@Override
	public Instituicao buscar(double id) {
		for(int i = 0;i<lista.size();i++) {
			if(Validar.pegarNumeros(lista.get(i).getCnpj())==id) {
				return lista.get(i);
			}
		}
		return null;

	}
	private void salvar() {
		DBInstituicao.salva(lista);
	}

	private ManipularInstituicao() {
		DBInstituicao = new DBInstituicao();
		lista = (ArrayList<Instituicao>) DBInstituicao.recupera();

	}

	public static ManipularInstituicao getInstace() {
		if (instance == null) {
			synchronized (ManipularInstituicao.class) {
				if (instance == null) {
					instance = new ManipularInstituicao();
				}
			}
		}
		return instance;
	}

}

package entidades;

public class Instituicao {

	private String cnpj;
	private double contato;
	private String razao, endereco;
	private String email;

	public Instituicao(String cnpj2, String razao, String endereco, String email, double contato) {
		setCnpj(cnpj2);
		setRazao(razao);
		setEndereco(endereco);
		setEmail(email);
		setContato(contato);
	}

	public Instituicao() {
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazao() {
		return razao;
	}

	public void setRazao(String razao) {
		this.razao = razao;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getContato() {
		return contato;
	}

	public void setContato(double contato) {
		this.contato = contato;
	}

	

}

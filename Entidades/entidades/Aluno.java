package entidades;

public class Aluno {

	private String nome;
	private String matricula;
	private int periodo;
	private String curso;

	public Aluno(String nome, String matricula, int periodo, String curso) {
		setNome(nome);
		setMatricula(matricula);
		setPeriodo(periodo);
		setCurso(curso);

	}

	public Aluno() {
		// TODO Auto-generated constructor stub
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String d) {
		this.matricula = d;
	}

	public int getPeriodo() {
		return periodo;
	}

	public void setPeriodo(int periodo) {
		this.periodo = periodo;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String toString() {
		return getNome();
	}

}

package entidades;

public class Curso {
	
	private int id;
	private String nome;
	
	public Curso(String nome,int id) {
		setNome(nome);
		setId(id);
	}
	public Curso() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String toString() {
		return getNome();
	}
	
	

}

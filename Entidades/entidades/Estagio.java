package entidades;
import javax.xml.crypto.Data;

public class Estagio {
	
	private String aluno,instituicao;
	private String protocoloInicial,ProtocoloFinal;
	private int id, nota;
	private long dataInicial,dataFinal;
	private String dataProtocoloInicial,dataProtocoloFinal;
	private String matriculaProfessor;
	
	public String getMatriculaProfessor() {
		return matriculaProfessor;
	}

	public void setMatriculaProfessor(String matriculaProfessor) {
		this.matriculaProfessor = matriculaProfessor;
	}

	public Estagio(int id,String instituicao) {
		setId(id);
		setInstituicao(instituicao);	
	}
	
	public Estagio() {
	}
	
	public String getAluno() {
		return aluno;
	}
	public void setAluno(String aluno) {
		this.aluno = aluno;
	}
	public String getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(String intituicao) {
		this.instituicao = intituicao;
	}
	public int getNota() {
		return nota;
	}
	public void setNota(int nota) {
		this.nota = nota;
	}
	public double getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProtocoloInicial() {
		return protocoloInicial;
	}
	public void setProtocoloInicial(String protocoloInicial) {
		this.protocoloInicial = protocoloInicial;
	}
	public String getProtocoloFinal() {
		return ProtocoloFinal;
	}
	public void setProtocoloFinal(String protocoloFinal) {
		ProtocoloFinal = protocoloFinal;
	}
	public long getDataInicial() {
		return dataInicial;
	}
	public void setDataInicial(long dataInicial) {
		this.dataInicial = dataInicial;
	}
	public long getDataFinal() {
		return dataFinal;
	}
	public void setDataFinal(long dataFinal) {
		this.dataFinal = dataFinal;
	}
	public String getDataProtocoloInicial() {
		return dataProtocoloInicial;
	}
	public void setDataProtocoloInicial(String dataProtocoloInicial) {
		this.dataProtocoloInicial = dataProtocoloInicial;
	}
	public String getDataProtocoloFinal() {
		return dataProtocoloFinal;
	}
	public void setDataProtocoloFinal(String dataProtocoloFinal) {
		this.dataProtocoloFinal = dataProtocoloFinal;
	}
}

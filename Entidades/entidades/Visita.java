package entidades;
import java.sql.Date;

public class Visita {
	
	private float dataDaVisita;
	private String relatorio;
	private int estagio;
	
	public float getDataDaVisita() {
		return dataDaVisita;
	}
	public void setDataDaVisita(float dataDaVisita) {
		this.dataDaVisita = dataDaVisita;
	}
	public String getRelatorio() {
		return relatorio;
	}
	public void setRelatorio(String relatorio) {
		this.relatorio = relatorio;
	}
	public int getEstagio() {
		return estagio;
	}
	public void setEstagio(int estagio) {
		this.estagio = estagio;
	}
	
	
	

}

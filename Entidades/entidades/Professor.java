package entidades;

public abstract class Professor {
	
	private String matricula;
	private String nome;
	
	/**Verificar se objeto e do tipo orientador ou coordenador
	 * 1 - se o objeto for Coordenador
	 * 2 - se o objeto for Orientador
	 * 
	 * @return tipo do objeto
	 */
	public abstract int getTipo();

	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String toString() {
		return nome;
	}
	
	
	
	

}

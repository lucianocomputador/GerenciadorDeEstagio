package principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import JDialog.GerenciarAlunos;
import JDialog.GerenciarInstituicoes;
import JDialog.GerenciarVisitas;
import JDialog.Info;
import JDialog.dialogCadastrar.DialogCadastrarOrientador;
import JDialog.dialogCadastrar.DialogCadastrarCurso;
import JDialog.dialogCadastrar.DialogCadastrarEstagio;
import Tabelas.TableModelAluno;
import Tabelas.TableModelEstagio;
import entidades.Aluno;
import entidades.Curso;
import entidades.Estagio;
import entidades.Instituicao;
import gerirDados.ManipularEstagio;
import gerirDados.ManipularInstituicao;

import com.jgoodies.forms.layout.FormSpecs;
import net.miginfocom.swing.MigLayout;
import percistencia.DBCurso;
import percistencia.DBEstagio;
import percistencia.StrategyDB;
import validar.SequencialId;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import java.awt.Color;

public class Home extends JFrame {

	private JPanel contentPane;

	private JMenuBar menuBar;
	private JMenu mnGerenciar;
	private JMenuItem mntmAlunos;
	private JMenuItem mntmInstituicoes;
	private JMenu mnOutros;
	private JMenuItem mntmSobre;
	private JPanel panel;
	private JPanel panel_2;

	private static Home instance;

	private JTabbedPane tabbedPane;
	private JPanel pnArea;

	private JButton btnRemoverEstagio;
	private JButton btnEstagio;
	private JMenuItem mntmNovoOrientador;
	private JMenuItem mntmCurso;

	private JTable JTEstagio;
	private TableModelEstagio modelTabela;
	private JScrollPane scrolAluno = new JScrollPane();
	private JButton btnVerVisitas;

	private DBEstagio dbEstagio = new DBEstagio();

	public void init() {
		menuBar = new JMenuBar();
		mnGerenciar = new JMenu("Gerenciar");
		mntmAlunos = new JMenuItem("Alunos");
		mntmInstituicoes = new JMenuItem("Instituicoes");
		mnOutros = new JMenu("Outros");
		mntmSobre = new JMenuItem("Sobre");
		panel = new JPanel();
		panel_2 = new JPanel();
		panel_2.setBackground(Color.GRAY);
		btnVerVisitas = new JButton("Gerenciar Visitas");

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		JTEstagio = new JTable();

		pnArea = new JPanel();

		btnRemoverEstagio = new JButton("Remover");
		btnEstagio = new JButton("Novo Estagio");
		mntmNovoOrientador = new JMenuItem("Orientador");
		mntmNovoOrientador.setSelected(true);
		mntmCurso = new JMenuItem("Curso");

	}

	/**
	 * Create the frame.
	 */
	private Home() {
		init();
		ouvintes();

		setVisible(true);
		setTitle("Gerenciador de estagios");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 988, 710);

		setJMenuBar(menuBar);

		menuBar.add(mnGerenciar);
		mnGerenciar.add(mntmAlunos);
		mnGerenciar.add(mntmInstituicoes);
		mnGerenciar.add(mntmNovoOrientador);
		mnGerenciar.add(mntmCurso);
		menuBar.add(mnOutros);

		mnOutros.add(mntmSobre);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		atualizar();

		panel.add(scrolAluno, BorderLayout.CENTER);
		JTEstagio.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE))
						.addGap(0)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)));
		// panel.setLayout(new BorderLayout(0, 0));

		// panel.add(tabbedPane);

		tabbedPane.addTab("Estagios", null, pnArea, null);
		pnArea.setLayout(new BorderLayout(0, 0));

		btnEstagio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		GroupLayout gl_panel_2 = new GroupLayout(panel_2);

		gl_panel_2.setHorizontalGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addComponent(btnEstagio, GroupLayout.PREFERRED_SIZE, 217, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnRemoverEstagio, GroupLayout.PREFERRED_SIZE, 201, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 336, Short.MAX_VALUE)
						.addComponent(btnVerVisitas, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)));
		gl_panel_2.setVerticalGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)

				.addGroup(gl_panel_2.createSequentialGroup()
						.addComponent(btnEstagio, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnRemoverEstagio, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 308, Short.MAX_VALUE)
						.addComponent(btnVerVisitas, GroupLayout.PREFERRED_SIZE, 218, GroupLayout.PREFERRED_SIZE)));
		gl_panel_2.setVerticalGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
						.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnEstagio, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)

								.addComponent(btnVerVisitas, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnRemoverEstagio, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE))
						.addGap(18)));

		panel_2.setLayout(gl_panel_2);
		contentPane.setLayout(gl_contentPane);
		// setFocusTraversalPolicy(new FocusTraversalOnArray(
		// new Component[] { contentPane, panel_2, btnEstagio, btnRemoverEstagio, panel,
		// tabbedPane, pnArea }));

		setLocationRelativeTo(null);
	}

	public void atualizar() {

		JTEstagio.setModel(modelTabela = new TableModelEstagio((ArrayList<Estagio>) dbEstagio.recupera()));
		scrolAluno.setViewportView(JTEstagio);

		JTEstagio.getTableHeader().setReorderingAllowed(false);

		panel.setLayout(new BorderLayout());
	}

	public void ouvintes() {
		mntmAlunos.addActionListener(new Ouvinte());

		mntmInstituicoes.addActionListener(new Ouvinte());

		btnEstagio.addActionListener(new Ouvinte());

		mntmCurso.addActionListener(new Ouvinte());

		mntmNovoOrientador.addActionListener(new Ouvinte());

		btnVerVisitas.addActionListener(new Ouvinte());

		btnRemoverEstagio.addActionListener(new Ouvinte());
		mntmSobre.addActionListener(new Ouvinte());

	}

	public class Ouvinte implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnRemoverEstagio) {
				Estagio a = modelTabela.getEstagio(JTEstagio.getSelectedRow());
				int op = JOptionPane.showConfirmDialog(null, "Realmente deseja remover este Estagio do Registro",
						"Remover Estagio", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (op == 0) {
					modelTabela.removerAluno(JTEstagio.getSelectedRow());
					ManipularEstagio.getInstace().remover(a);
				}
			}
			if (e.getSource() == btnEstagio) {
				new DialogCadastrarEstagio();

			}
			if (e.getSource() == btnVerVisitas) {
				if (JTEstagio.getSelectedRow() != -1) {
					Estagio est = modelTabela.getEstagio(JTEstagio.getSelectedRow());
					new GerenciarVisitas(est);
				} else {
					JOptionPane.showMessageDialog(null, "Um Estagio deve ser Selecionado");
				}

			}
			if (e.getSource() == mntmNovoOrientador) {
				new DialogCadastrarOrientador();
			}

			if (e.getSource() == mntmCurso) {
				new DialogCadastrarCurso();
			}
			if (e.getSource() == mntmInstituicoes) {
				GerenciarInstituicoes.getInstance();
			}
			if (e.getSource() == mntmAlunos) {
				GerenciarAlunos.getInstance();
			}
			if (e.getSource() == mntmSobre) {
				new Info();
			}

		}

	}

	public static Home getInstance() {
		if (instance == null) {
			synchronized (Home.class) {
				if (instance == null) {
					instance = new Home();
				}
			}
		}
		return instance;
	}

}

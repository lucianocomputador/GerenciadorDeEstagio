package JDialog.dialogCadastrar;




import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Impressao.ListaEstagio;
import Impressao.RelEstagioPorAluno;
import Impressao.RelEstagioPorInstituicao;
import entidades.Aluno;
import entidades.Estagio;
import entidades.Instituicao;
import entidades.Orientador;
import percistencia.DBAluno;
import percistencia.DBInstituicao;
import percistencia.DBOrientador;
import percistencia.StrategyDB;


public class DialogPesquisarEstagio extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private JComboBox cbxAluno;
	private JComboBox cbxInstituicao;
	private JComboBox cbxOrientador;
	
	private JButton btnVisualizarAluno;
	private JButton btnVisualizarInstituicao;
	private JButton btnVisualizarOrientador;
	
	/**
	 * Chama banco de dado para SALVAR e RECUPARAR dados
	 */

	private ArrayList<Estagio> listaEstagio = new ArrayList<Estagio>();

	private StrategyDB<Instituicao> dbinst = new DBInstituicao();
	private StrategyDB<Aluno> dbaluno = new DBAluno();
	private StrategyDB<Orientador> dbOrientador = new DBOrientador();

	private ArrayList<Instituicao> listaInstituicao = new ArrayList<>();
	private ArrayList<Aluno> listaAluno = new ArrayList<>();
	private ArrayList<Orientador> listaOrientador = new ArrayList<>();
	

	
	private JTextField txtPerIniDtIni;
	private JTextField txtPerIniDtFim;
	private JButton btnVisualizarPorDataFinal;
	private JButton btnListaAlunos;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DialogPesquisarEstagio dialog = new DialogPesquisarEstagio();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DialogPesquisarEstagio() {
		setTitle("Pesquisa Estágio");
		setBounds(100, 100, 550, 399);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 550, 375);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		cbxAluno = new JComboBox();
		cbxAluno.setBounds(12, 32, 358, 41);
		contentPanel.add(cbxAluno);
		
		JLabel label = new JLabel("Aluno");
		label.setHorizontalAlignment(SwingConstants.LEFT);
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		label.setBounds(12, 12, 80, 20);
		contentPanel.add(label);
		
		JLabel label_1 = new JLabel("Instituicao");
		label_1.setHorizontalAlignment(SwingConstants.LEFT);
		label_1.setFont(new Font("Dialog", Font.BOLD, 14));
		label_1.setBounds(12, 79, 162, 20);
		contentPanel.add(label_1);
		
		cbxInstituicao = new JComboBox();
		cbxInstituicao.setBounds(12, 99, 358, 41);
		contentPanel.add(cbxInstituicao);
		
		JLabel label_2 = new JLabel("Orientador");
		label_2.setFont(new Font("Dialog", Font.BOLD, 14));
		label_2.setBounds(12, 151, 90, 14);
		contentPanel.add(label_2);
		
		cbxOrientador = new JComboBox();
		cbxOrientador.setBounds(12, 165, 358, 41);
		contentPanel.add(cbxOrientador);
		
		btnVisualizarAluno = new JButton("Visualizar");
		btnVisualizarAluno.setBounds(386, 40, 117, 25);
		btnVisualizarAluno.addActionListener(new Ouvinte());
		contentPanel.add(btnVisualizarAluno);
		
		btnVisualizarInstituicao = new JButton("Visualizar");
		btnVisualizarInstituicao.setBounds(386, 107, 117, 25);
		btnVisualizarInstituicao.addActionListener(new Ouvinte());
		contentPanel.add(btnVisualizarInstituicao);
		
		btnVisualizarOrientador = new JButton("Visualizar");
		btnVisualizarOrientador.setBounds(386, 173, 117, 25);
		btnVisualizarOrientador.addActionListener(new Ouvinte());
		contentPanel.add(btnVisualizarOrientador);
		
		JPanel panel_inicio = new JPanel();
		panel_inicio.setBounds(12, 218, 491, 73);
		contentPanel.add(panel_inicio);
		panel_inicio.setLayout(null);
		
		JLabel lblDataInicio = new JLabel("Data Início:");
		lblDataInicio.setBounds(12, 9, 83, 15);
		panel_inicio.add(lblDataInicio);
		
		txtPerIniDtIni = new JTextField();
		txtPerIniDtIni.setBounds(110, 7, 114, 19);
		panel_inicio.add(txtPerIniDtIni);
		txtPerIniDtIni.setColumns(10);
		
		JLabel lblDataFinal = new JLabel("Data Final:");
		lblDataFinal.setBounds(242, 9, 83, 15);
		panel_inicio.add(lblDataFinal);
		
		txtPerIniDtFim = new JTextField();
		txtPerIniDtFim.setBounds(343, 7, 114, 19);
		panel_inicio.add(txtPerIniDtFim);
		txtPerIniDtFim.setColumns(10);
		
		JButton btnVisualizarPordataInicio = new JButton("Visualizar por Data Início");
		btnVisualizarPordataInicio.setBounds(12, 36, 212, 25);
		btnVisualizarPordataInicio.addActionListener(new Ouvinte());
		panel_inicio.add(btnVisualizarPordataInicio);
		
		btnVisualizarPorDataFinal = new JButton("Visualizar por Data Final");
		btnVisualizarPorDataFinal.setBounds(242, 36, 215, 25);
		btnVisualizarPorDataFinal.addActionListener(new Ouvinte());
		panel_inicio.add(btnVisualizarPorDataFinal);
		
		btnListaAlunos = new JButton("Lista dos Alunos em Estágio");
		btnListaAlunos.setBounds(255, 315, 248, 25);
		btnListaAlunos.addActionListener(new Ouvinte());
		contentPanel.add(btnListaAlunos);
		
		
		// povoar combo box instituição
		listaInstituicao = (ArrayList<Instituicao>) dbinst.recupera();
		for (Instituicao inst : listaInstituicao) {
			cbxInstituicao.addItem(inst.getRazao());
		}

		// povoar combo box aluno
		listaAluno = (ArrayList<Aluno>) dbaluno.recupera();
		for (Aluno alu : listaAluno) {
			cbxAluno.addItem(alu);
		}
		
		// povoar combo box orientador
		listaOrientador = (ArrayList<Orientador>) dbOrientador.recupera();
		for (Orientador alu : listaOrientador) {
			cbxOrientador.addItem(alu);
		}

	}
	
	public class Ouvinte implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			
			if (e.getSource() == btnVisualizarAluno) {

				new RelEstagioPorAluno();
				
			}
			
			if (e.getSource() == btnVisualizarInstituicao) {

				new RelEstagioPorInstituicao();
				
			}
			
			if (e.getSource() == btnListaAlunos) {

				new ListaEstagio();
				
			}


		}
	}
}

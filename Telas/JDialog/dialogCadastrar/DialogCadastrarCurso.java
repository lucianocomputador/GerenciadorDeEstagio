package JDialog.dialogCadastrar;

import javax.swing.JDialog;

import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import JDialog.GerenciarAlunos;
import JDialog.GerenciarVisitas;
import Tabelas.TableModelAluno;
import Tabelas.TableModelCurso;
import entidades.Aluno;
import entidades.Curso;
import entidades.Estagio;
import entidades.Orientador;
import gerirDados.ManipularCurso;
import gerirDados.ManipularOrientador;
import percistencia.DBAluno;
import percistencia.DBCurso;
import percistencia.DBOrientador;
import percistencia.StrategyDB;
import validar.SequencialId;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class DialogCadastrarCurso extends JDialog {

	private static final long serialVersionUID = 1L;
	private JTextField txCurso;
	private JButton btSalvar;
	private JButton btRemover;

	private StrategyDB db = new DBOrientador();
 
	private JPanel panel;
	private JPanel pnArea;
	
	
	private JTable JTCurso;
	private TableModelCurso modelTabela;
	private JScrollPane scrolAluno = new JScrollPane();
	private StrategyDB<Curso> bdA = new DBCurso();
	

	/**
	 * Launch the application.
	 */

	private void iniciar() {
		pnArea = new JPanel();
		pnArea.setBorder(new LineBorder(Color.GRAY));
	}
	public DialogCadastrarCurso() {
		iniciar();
		// recupera dados.
		

		setTitle("Cadatrar Orientador");
		setBounds(100, 100, 644, 547);
		setLocationRelativeTo(null);

		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));

		txCurso = new JTextField();
		txCurso.setColumns(10);

		JLabel lblNewLabel = new JLabel("Nome do Curso");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		btSalvar = new JButton("Salvar");
		btSalvar.addActionListener(new Ouvinte());

		btRemover = new JButton("Remover");
		btRemover.addActionListener(new Ouvinte());
		
		JTCurso = new JTable();
		JTCurso.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		AtualizarTabela();
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(btSalvar, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
						.addComponent(txCurso, GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(txCurso, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(7)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(btSalvar, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		panel.setLayout(gl_panel);

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(10)
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addGap(10))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap()
								.addComponent(pnArea, GroupLayout.PREFERRED_SIZE, 619, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(11)
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(pnArea, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE).addContainerGap()));
		getContentPane().setLayout(groupLayout);

		setModal(true);
		setResizable(false);
		setVisible(true);

	}
	
	public void AtualizarTabela() {
		JTCurso = new JTable();
		JTCurso.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		JTCurso.setModel(modelTabela = new TableModelCurso( (ArrayList<Curso>) bdA.recupera()));
		scrolAluno.setViewportView(JTCurso);
		
		JTCurso.getTableHeader().setReorderingAllowed(false);
		pnArea.setLayout(new BorderLayout(0, 0));
		pnArea.add(scrolAluno);
	}

	private class Ouvinte implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			ArrayList<Curso> listaCurso = (ArrayList<Curso>) bdA.recupera();

			if (e.getSource()==btSalvar) {
				if (txCurso.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Preencha os campos de texto corretamente");
				} else {
					Curso novo = new Curso();
					novo.setNome(txCurso.getText());
					novo.setId(SequencialId.sequencialIdCurso((ArrayList<Curso>) bdA.recupera()));

					listaCurso.add(novo);
					bdA.salva(listaCurso);
					
					txCurso.setText("");
					
					modelTabela.addAluno(novo);

				}
			} if (e.getSource()==btRemover) {
				
				if (JTCurso.getSelectedRow() != -1) {
					Curso a = modelTabela.getCurso(JTCurso.getSelectedRow());
					int op = JOptionPane.showConfirmDialog(null, "Realmente deseja remover o Curso: "+a.getNome(),"Remover Curso",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					if(op==0) {
						modelTabela.removerCurso(JTCurso.getSelectedRow());
						ManipularCurso.getInstace().remover(a);					
					}
				} else {
					JOptionPane.showMessageDialog(null, "Um Curso deve ser Selecionado");
				}
				
				
				
			}
		}
	}
}

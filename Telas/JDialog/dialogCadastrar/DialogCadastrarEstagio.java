package JDialog.dialogCadastrar;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.sound.midi.Sequence;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.xml.crypto.Data;

import com.thoughtworks.xstream.core.SequenceGenerator;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;

import JDialog.GerenciarAlunos;
import entidades.Aluno;
import entidades.Curso;
import entidades.Estagio;
import entidades.Instituicao;
import entidades.Orientador;
import entidades.Professor;
import gerirDados.ManipularEstagio;
import jdk.internal.org.objectweb.asm.commons.SerialVersionUIDAdder;
import percistencia.DBAluno;
import percistencia.DBEstagio;
import percistencia.DBInstituicao;
import percistencia.DBOrientador;
import percistencia.StrategyDB;
import principal.Home;
import sun.security.util.HostnameChecker;
import validar.Validar;
import validar.SequencialId;

import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.border.LineBorder;
import javax.swing.text.MaskFormatter;

import java.awt.Color;

public class DialogCadastrarEstagio extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JFormattedTextField txNota;
	private JFormattedTextField txProcessoInicial;
	private JFormattedTextField txProcessofin;

	private JSpinner spnDataInicio = new JSpinner();
	private JSpinner spnDataFin = new JSpinner();
	private JButton btnSalvar;
	private JButton btnCancelar;
	private JComboBox cbxInstituicao = new JComboBox();
	private JComboBox cbxAlunos = new JComboBox();
	private JComboBox cbxOrientador = new JComboBox();

	private SpinnerDateModel spinModI = new SpinnerDateModel();
	private SpinnerDateModel spinModF = new SpinnerDateModel();
	/**
	 * Chama banco de dado para SALVAR e RECUPARAR dados
	 */

	private ArrayList<Estagio> listaEstagio = new ArrayList<Estagio>();

	private StrategyDB<Instituicao> dbinst = new DBInstituicao();
	private StrategyDB<Aluno> dbaluno = new DBAluno();
	private StrategyDB<Orientador> dbOrientador = new DBOrientador();

	private ArrayList<Instituicao> listaInstituicao = new ArrayList<>();
	private ArrayList<Aluno> listaAluno = new ArrayList<>();
	private ArrayList<Orientador> listaOrientador = new ArrayList<>();

	public DialogCadastrarEstagio() {
		btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(136, 502, 117, 30);
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(259, 502, 117, 30);


		setTitle("Cadatrar Estagio");
		setBounds(100, 100, 392, 572);
		setLocationRelativeTo(null);

		JLabel lblInstituicao = new JLabel("Instituicao");
		lblInstituicao.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblInstituicao.setBounds(18, 78, 162, 20);
		lblInstituicao.setHorizontalAlignment(SwingConstants.LEFT);

		JLabel lblAluno = new JLabel("Aluno");
		lblAluno.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAluno.setBounds(18, 11, 80, 20);
		lblAluno.setHorizontalAlignment(SwingConstants.LEFT);

		JLabel lblProcessoInicial = new JLabel("Processo Inicial:");
		lblProcessoInicial.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblProcessoInicial.setBounds(25, 328, 155, 30);
		lblProcessoInicial.setHorizontalAlignment(SwingConstants.RIGHT);

		JLabel label_3 = new JLabel("Data Inicio:");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_3.setBounds(56, 243, 124, 20);
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);

		JLabel label_4 = new JLabel("Data Fim:");
		label_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_4.setBounds(100, 284, 80, 20);
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);

		JLabel lblNota = new JLabel("Nota:");
		lblNota.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNota.setBounds(131, 418, 49, 15);
		lblNota.setHorizontalAlignment(SwingConstants.RIGHT);

		JLabel lblProcessoFinal = new JLabel("Processo Final:");
		lblProcessoFinal.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblProcessoFinal.setBounds(10, 373, 170, 20);
		lblProcessoFinal.setHorizontalAlignment(SwingConstants.RIGHT);

		txNota = new JFormattedTextField();
		Validar.setMascara("###", txNota,"0123456789");
		txNota.setBounds(184, 410, 184, 30);
		txNota.setColumns(10);

		// povoar combo box instituição
		listaInstituicao = (ArrayList<Instituicao>) dbinst.recupera();
		for (Instituicao inst : listaInstituicao) {
			cbxInstituicao.addItem(inst.getRazao());
		}

		listaAluno = (ArrayList<Aluno>) dbaluno.recupera();
		for (Aluno alu : listaAluno) {
			cbxAlunos.addItem(alu);
		}
		listaOrientador = (ArrayList<Orientador>) dbOrientador.recupera();
		for (Orientador alu : listaOrientador) {
			cbxOrientador.addItem(alu);
		}

		btnSalvar.addActionListener(new Ouvinte());

		spnDataInicio = new JSpinner(spinModI);
		spnDataInicio.setBounds(184, 239, 184, 30);
		spnDataFin = new JSpinner(spinModF);
		spnDataFin.setBounds(184, 280, 184, 30);

		spnDataInicio.setEditor(new JSpinner.DateEditor(spnDataInicio, "dd/MM/yyyy"));
		spnDataFin.setEditor(new JSpinner.DateEditor(spnDataFin, "dd/MM/yyyy"));

		txProcessoInicial = new JFormattedTextField();
		Validar.setMascara("#####.######.####-##", txProcessoInicial,"0123456789");
		
		txProcessoInicial.setBounds(184, 328, 184, 30);
		txProcessoInicial.setColumns(20);

		txProcessofin = new JFormattedTextField();
		Validar.setMascara("#####.######.####-##", txProcessofin,"0123456789");
		txProcessofin.setBounds(184, 366, 184, 30);
		txProcessofin.setColumns(10);

		getContentPane().setLayout(null);
		cbxAlunos.setBounds(18, 31, 358, 41);
		getContentPane().add(cbxAlunos);
		getContentPane().add(lblAluno);
		getContentPane().add(lblInstituicao);
		getContentPane().add(label_3);
		getContentPane().add(label_4);
		getContentPane().add(lblProcessoInicial);
		getContentPane().add(lblProcessoFinal);
		getContentPane().add(lblNota);
		getContentPane().add(txProcessofin);
		getContentPane().add(spnDataInicio);
		getContentPane().add(spnDataFin);
		getContentPane().add(txProcessoInicial);
		getContentPane().add(txNota);
		cbxInstituicao.setBounds(18, 98, 358, 41);
		getContentPane().add(cbxInstituicao);
		getContentPane().add(btnSalvar);
		getContentPane().add(btnCancelar);

		cbxOrientador.setBounds(18, 164, 358, 41);
		getContentPane().add(cbxOrientador);

		JLabel lblOrientador = new JLabel("Orientador");
		lblOrientador.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblOrientador.setBounds(18, 150, 90, 14);
		getContentPane().add(lblOrientador);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setBounds(10, 227, 366, 257);
		getContentPane().add(panel);
		panel.setLayout(null);

		setModal(true);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);

	}

	public class Ouvinte implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == btnSalvar) {

				if (Validar.isPreechido(txProcessoInicial) || Validar.isPreechido(txProcessofin)
						|| Validar.isPreechido(txNota)) {
					JOptionPane.showMessageDialog(null, "Faltam Campus a serem Preechidos");
				} else {

					// Cria estagio
					Estagio estagio = new Estagio();

					Aluno aluno = (Aluno) cbxAlunos.getSelectedItem();
					estagio.setId(SequencialId.sequencialIdEstagio(ManipularEstagio.getInstace().listaTodos()));
					estagio.setAluno(aluno.getNome());
					estagio.setInstituicao((String) cbxInstituicao.getSelectedItem());
					estagio.setNota(Integer.parseInt(txNota.getText()));
					estagio.setProtocoloInicial(txProcessoInicial.getText());
					estagio.setProtocoloFinal(txProcessofin.getText());

					Orientador prof = (Orientador) cbxOrientador.getSelectedItem();

					estagio.setMatriculaProfessor(prof.getMatricula());

					Date inicio = (Date) spnDataInicio.getValue();
					Date fim = (Date) spnDataFin.getValue();

					estagio.setDataInicial(inicio.getTime());
					estagio.setDataFinal(fim.getTime());
					// Adiciona na lista de aluno
					ManipularEstagio.getInstace().add(estagio);

					// Salva no XML
					

					Home.getInstance().atualizar();

					dispose();
				}

			}
			if (e.getSource() == btnCancelar) {
				dispose();
			}

		}

	}
}
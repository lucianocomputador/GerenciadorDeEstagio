package JDialog.dialogCadastrar;

import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import JDialog.GerenciarAlunos;
import Tabelas.TableModelAluno;
import Tabelas.TableModelOrientador;
import entidades.Aluno;
import entidades.Curso;
import entidades.Orientador;
import gerirDados.ManipularAluno;
import gerirDados.ManipularOrientador;
import percistencia.DBAluno;
import percistencia.DBOrientador;
import percistencia.StrategyDB;
import validar.Validar;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class DialogCadastrarOrientador extends JDialog {

	private static final long serialVersionUID = 1L;
	private JFormattedTextField txMatricula;
	private JTextField txOrientador;
	private JButton btSalvar;
	private JButton btRemover;

	private JPanel panel;
	private JPanel pnArea;

	private JTable JTOrientador;
	private TableModelOrientador modelTabela;

	private JScrollPane scrolAluno = new JScrollPane();

	/**
	 * Launch the application.
	 */

	private void iniciar() {
		pnArea = new JPanel();
		pnArea.setBorder(new LineBorder(Color.GRAY));
	}

	public DialogCadastrarOrientador() {

		iniciar();

		setTitle("Cadatrar Orientador");
		setBounds(100, 100, 644, 547);
		setLocationRelativeTo(null);

		AtualizarTabela();

		panel = new JPanel();
		panel.setBorder(new LineBorder(Color.GRAY));

		txOrientador = new JTextField();
		txOrientador.setColumns(10);

		JLabel lblNewLabel = new JLabel("Nome do Orientador");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);

		txMatricula = new JFormattedTextField();
		Validar.setMascara("############", txMatricula, "0123456789");
		txMatricula.setColumns(10);

		JLabel lblMatricula = new JLabel("Matricula");
		lblMatricula.setHorizontalAlignment(SwingConstants.RIGHT);

		btSalvar = new JButton("Salvar");
		btSalvar.addActionListener(new Ouvinte());

		btRemover = new JButton("Remover");
		btRemover.addActionListener(new Ouvinte());

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblMatricula, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup()
						.addComponent(txMatricula, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
						.addComponent(btSalvar, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE).addGap(4)
						.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
						.addComponent(txOrientador, GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE))
				.addContainerGap()));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(txOrientador, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
						.addGap(7)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(btSalvar, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
								.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblMatricula, GroupLayout.PREFERRED_SIZE, 23,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(txMatricula, GroupLayout.PREFERRED_SIZE, 31,
												GroupLayout.PREFERRED_SIZE)))
						.addContainerGap()));
		panel.setLayout(gl_panel);

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(pnArea,
								GroupLayout.DEFAULT_SIZE, 619, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup().addGap(9).addComponent(panel,
								GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
				.addGap(9)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(pnArea, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE).addContainerGap()));
		getContentPane().setLayout(groupLayout);

		setModal(true);
		setResizable(false);
		setVisible(true);

	}

	public void AtualizarTabela() {

		JTOrientador = new JTable();
		JTOrientador.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JTOrientador.setModel(modelTabela = new TableModelOrientador(ManipularOrientador.getInstace().listaTodos()));
		scrolAluno.setViewportView(JTOrientador);

		JTOrientador.getTableHeader().setReorderingAllowed(false);
		pnArea.setLayout(new BorderLayout(0, 0));
		pnArea.add(scrolAluno);
	}

	private class Ouvinte implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == btSalvar) {
				if (txMatricula.getText().equals("") || txOrientador.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Preencha os campos de texto corretamente");
				} else {

					if (ManipularOrientador.getInstace().buscar(Validar.pegarNumeros(txMatricula.getText())) == null) {
						Orientador novo = new Orientador();
						novo.setNome(txOrientador.getText());
						novo.setMatricula(txMatricula.getText());

						ManipularOrientador.getInstace().add(novo);
						modelTabela.addOrientador(novo);

					} else {
						JOptionPane.showMessageDialog(null, "Orientador ja cadastrado");
					}

				}
			}
			if (e.getSource() == btRemover) {
				Orientador a = modelTabela.getOrientador(JTOrientador.getSelectedRow());
				int op = JOptionPane.showConfirmDialog(null, "Realmente deseja remover o Orientador: " + a.getNome(),
						"Remover Aluno", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (op == 0) {
					modelTabela.removerOrientador(JTOrientador.getSelectedRow());
					ManipularOrientador.getInstace().remover(a);
				}

			}
		}
	}

}

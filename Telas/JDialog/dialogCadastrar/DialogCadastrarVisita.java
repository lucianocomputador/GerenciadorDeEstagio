package JDialog.dialogCadastrar;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.awt.Choice;
import javax.swing.JToggleButton;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.xml.crypto.Data;

import JDialog.GerenciarVisitas;
import entidades.Aluno;
import entidades.Estagio;
import entidades.Visita;
import gerirDados.Manipuladores;
import gerirDados.ManipularOrientador;
import gerirDados.ManipularVisita;
import percistencia.DBVisita;
import percistencia.StrategyDB;
import validar.Validar;

import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

public class DialogCadastrarVisita extends JDialog {

	private final JPanel contentPanel = new JPanel();

	private JLabel aluno = new JLabel("Aluno");
	private JPanel panel = new JPanel();
	private JScrollPane scrollPane = new JScrollPane();
	private JTextArea txtRelatorio = new JTextArea();
	private JButton btnSalvar = new JButton("Salvar");
	private JButton btnCancelar = new JButton("Cancelar");
	private ArrayList<Visita> lista = new ArrayList<>();
	private Estagio esta = new Estagio();
	private JLabel lblOrientador;
	

	public DialogCadastrarVisita(Estagio est) {
		
		btnSalvar.addActionListener(new ouvinte());
		btnCancelar.addActionListener(new ouvinte());
		
		setModal(true);
		esta = est;
		setTitle("Registrar relatorio da Vsita Tecnica ao estagio");
		setResizable(false);
		setBounds(100, 100, 800, 500);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		aluno.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 18));
		aluno.setBounds(10, 11, 774, 34);
		aluno.setText(est.getAluno());
		contentPanel.add(aluno);

		panel.setBounds(10, 56, 475, 404);
		contentPanel.add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		panel.add(scrollPane, BorderLayout.CENTER);
		txtRelatorio.setWrapStyleWord(true);
		txtRelatorio.setLineWrap(true);
		txtRelatorio.setFont(new Font("Times New Roman", Font.PLAIN, 16));

		scrollPane.setViewportView(txtRelatorio);
		
		btnSalvar.setBounds(530, 420, 122, 40);
		contentPanel.add(btnSalvar);

		

		btnCancelar.setBounds(662, 420, 122, 40);
		contentPanel.add(btnCancelar);
		
		JLabel lblEmpresa = new JLabel("Empresa:"+est.getInstituicao());
		lblEmpresa.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblEmpresa.setBounds(495, 56, 289, 40);
		contentPanel.add(lblEmpresa);
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		JLabel lblInicio = new JLabel("Inicio:"+formato.format(est.getDataInicial()));
		lblInicio.setHorizontalAlignment(SwingConstants.RIGHT);
		lblInicio.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblInicio.setBounds(495, 297, 289, 40);
		contentPanel.add(lblInicio);
		
		JLabel lblTermino = new JLabel("Termino:"+formato.format(est.getDataFinal()));
		lblTermino.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTermino.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTermino.setBounds(495, 335, 289, 40);
		contentPanel.add(lblTermino);
		
		lblOrientador = new JLabel("Orientador: "+ManipularOrientador.getInstace().buscar(Validar.pegarNumeros(est.getMatriculaProfessor())).getNome());
		lblOrientador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblOrientador.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblOrientador.setBounds(495, 249, 289, 40);
		
		contentPanel.add(lblOrientador);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public class ouvinte implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnSalvar) {
				
				

				Visita vis = new Visita();
				vis.setRelatorio(txtRelatorio.getText());
				vis.setEstagio((int)esta.getId());
				float data = Calendar.getInstance().getTime().getTime();
				vis.setDataDaVisita(data);
				lista = ManipularVisita.getInstace().listaTodos();
				ManipularVisita.getInstace().add(vis);
				dispose();
				

			}
			if (e.getSource() == btnCancelar) {
				dispose();
			}

		}

	}
}

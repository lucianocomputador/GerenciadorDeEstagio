package JDialog.dialogCadastrar;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import javax.swing.SwingConstants;

import JDialog.GerenciarAlunos;
import entidades.Aluno;
import entidades.Curso;
import entidades.Instituicao;
import gerirDados.ManipularAluno;
import percistencia.DBAluno;
import percistencia.DBCurso;
import percistencia.DBInstituicao;
import percistencia.StrategyDB;
import percistencia.XML;
import validar.Validar;
import java.awt.Font;

public class DialogCadastrarAluno extends JDialog {

	@SuppressWarnings({ "rawtypes" })

	/**
	 * Chama banco de dado para SALVAR e RECUPARAR dados
	 */

	private ArrayList<Aluno> listaAluno = new ArrayList<Aluno>();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JSpinner spnPeriodo;
	private JButton btnCancelar;
	private JButton btnSalvar;
	private JTextField txNome;
	private JFormattedTextField txMatricula;
	private JComboBox cbxCurso;
	
	private StrategyDB<Curso> dbCurso = new DBCurso();
	private ArrayList<Curso> listaCursos = new ArrayList<>();

	/**
	 * Launch the application.
	 */

	public DialogCadastrarAluno() {
		setResizable(false);

		setTitle("Cadatrar Aluno");
		setBounds(100, 100, 473, 334);

		cbxCurso = new JComboBox<Curso>();
		cbxCurso.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		cbxCurso.setBounds(10, 105, 447, 37);
		
		listaCursos = (ArrayList<Curso>) dbCurso.recupera();
		for (Curso inst : listaCursos) {
			cbxCurso.addItem(inst);
		}
		
		
		
		txNome = new JTextField();
		txNome.setFont(new Font("Arial", Font.BOLD, 14));
		txNome.setBounds(10, 41, 447, 37);
		txNome.setColumns(10);

		// recupera dados.

		btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(215, 259, 105, 33);
		btnSalvar.addActionListener(new ouvinte());
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(326, 260, 131, 30);
		btnCancelar.addActionListener(new ouvinte());

		JLabel label = new JLabel("Nome");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(10, 26, 72, 14);

		JLabel label_1 = new JLabel("Periodo");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(10, 177, 67, 14);

		spnPeriodo = new JSpinner();
		spnPeriodo.setBounds(81, 168, 55, 32);

		JLabel label_2 = new JLabel("Matricula");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_2.setBounds(231, 177, 89, 14);
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);

		txMatricula = new JFormattedTextField();
		txMatricula.setBounds(331, 163, 126, 37);
		Validar.setMascara("############", txMatricula, "0123456789");
		txMatricula.setColumns(10);

		JLabel label_3 = new JLabel("Curso");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label_3.setBounds(10, 89, 84, 14);
		getContentPane().setLayout(null);
		getContentPane().add(cbxCurso);
		getContentPane().add(txNome);
		getContentPane().add(label);
		getContentPane().add(label_3);
		getContentPane().add(label_1);
		getContentPane().add(spnPeriodo);
		getContentPane().add(label_2);
		getContentPane().add(btnSalvar);
		getContentPane().add(txMatricula);
		getContentPane().add(btnCancelar);

		setModal(true);
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public class ouvinte implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == btnSalvar) {

				if (txMatricula.getText().equals("") || txNome.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Falta compos e serem preechidos");
				} else {
					// Cria aluno
					
					if(ManipularAluno.getInstace().buscar(Validar.pegarNumeros(txMatricula.getText()))==null) {
						Aluno aluno = new Aluno();
						aluno.setMatricula(txMatricula.getText());
						aluno.setNome(txNome.getText());
						
						Curso cur = (Curso) cbxCurso.getSelectedItem();
						aluno.setCurso(cur.getNome());
						aluno.setPeriodo((int) spnPeriodo.getValue());
						ManipularAluno.getInstace().add(aluno);
						
						dispose();
						
						GerenciarAlunos.getInstance().AtualizarTabela();
						
					}else{
						JOptionPane.showMessageDialog(null, "Aluno ja cadastrado");
					}
		
				}
			}
			if (e.getSource() == btnCancelar) {
				dispose();
			}

		}

	}
}

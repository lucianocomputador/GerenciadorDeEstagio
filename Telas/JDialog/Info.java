package JDialog;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Label;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.BoxLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import net.miginfocom.swing.MigLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class Info extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ImageIcon ifpb  = new ImageIcon(getClass().getResource("/icon/IFPBMonteiro.jpg"));

	/**
	 * metodo construtor
	 */
	public Info() {
		setBounds(100, 100, 571, 600);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("");
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setIcon(ifpb);
			lblNewLabel.setBounds(10, 50, 545, 274);
			contentPanel.add(lblNewLabel);
		}
		{
			JLabel lblSistemaParaAlocao = new JLabel("Sistema Para o Gerenciamento de Estagios");
			lblSistemaParaAlocao.setFont(new Font("Tahoma", Font.PLAIN, 18));
			lblSistemaParaAlocao.setHorizontalAlignment(SwingConstants.CENTER);
			lblSistemaParaAlocao.setBounds(10, 11, 545, 40);
			contentPanel.add(lblSistemaParaAlocao);
		}
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.GRAY));
		panel.setBounds(10, 341, 545, 219);
		contentPanel.add(panel);
		panel.setLayout(new MigLayout("", "[46px][46px]", "[14px][][][][][][][]"));
		
		JLabel lblPadroesDeProjetos = new JLabel("Software desenvolvido para a disciplina de Processos de Desenvolvimento");
		lblPadroesDeProjetos.setFont(new Font("Tahoma", Font.BOLD, 14));
		panel.add(lblPadroesDeProjetos, "cell 0 0");
		
		JLabel lblNewLabel_2 = new JLabel("Desenvolvedores");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setFont(new Font("Comic Sans MS", Font.BOLD, 16));
		panel.add(lblNewLabel_2, "cell 0 2,alignx center,aligny center");
		
		JLabel lblNewLabel_1 = new JLabel("Igor De Carvalho Soares");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 13));
		panel.add(lblNewLabel_1, "cell 0 3,alignx left,aligny center");
		
		JLabel lblNewLabel_3 = new JLabel("Luciano Azevedo");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 13));
		panel.add(lblNewLabel_3, "cell 0 4");
		
		JLabel lblNewLabel_4 = new JLabel("Ernando J\u00FAnior");
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 13));
		panel.add(lblNewLabel_4, "cell 0 5");
		
		JLabel lblNewLabel_5 = new JLabel("Leonardo Araujo");
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 13));
		panel.add(lblNewLabel_5, "cell 0 6");
		
		JLabel lblNewLabel_6 = new JLabel("Marcos Junior");
		lblNewLabel_6.setFont(new Font("Arial", Font.BOLD, 13));
		panel.add(lblNewLabel_6, "cell 0 7");
		setModal(true);
		setTitle("Informações");
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		
	}
}

package JDialog;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.border.LineBorder;

import JDialog.dialogCadastrar.DialogCadastrarVisita;
import Tabelas.TableModelVisita;
import entidades.Aluno;
import entidades.Estagio;
import entidades.Visita;
import gerirDados.ManipularAluno;
import gerirDados.ManipularOrientador;
import gerirDados.ManipularVisita;
import percistencia.DBVisita;
import percistencia.StrategyDB;
import validar.Validar;

import java.awt.Color;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

public class GerenciarVisitas extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();

	private JButton btNovaVisita;
	private JButton btnEditar;
	private JButton btRemover;
	private JLabel lbAluno;
	private JPanel pnArea;

	private JTable JTVisitas;
	private TableModelVisita modelTabela;
	private JScrollPane scrolVisita = new JScrollPane();
	
	private Estagio estagio;
	private JLabel lblOrientador;
	
	
	public GerenciarVisitas(Estagio est) {
		estagio = est;
		setModal(true);
		
		setTitle("Gerenciar Visitas");
		setBounds(100, 100, 807, 598);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		pnArea = new JPanel();

		atualizarTabela();
		
		pnArea.add(scrolVisita, BorderLayout.CENTER);

		JPanel panel = new JPanel();

		btNovaVisita = new JButton("Nova Visita");

		btnEditar = new JButton("Editar");
		btnEditar.setEnabled(false);

		btRemover = new JButton("Remover");

		lbAluno = new JLabel(est.getAluno());
		
		lbAluno.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		
		JLabel lbEmpresa = new JLabel("Empresa: "+est.getInstituicao());
		lbEmpresa.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lbInicio = new JLabel("Inicio: "+formato.format(est.getDataInicial()));
		lbInicio.setHorizontalAlignment(SwingConstants.RIGHT);
		lbInicio.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lbtermino = new JLabel("Termino: "+formato.format(est.getDataFinal()));
		lbtermino.setHorizontalAlignment(SwingConstants.RIGHT);
		lbtermino.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		lblOrientador = new JLabel("Orientador: "+ManipularOrientador.getInstace().buscar(Validar.pegarNumeros(est.getMatriculaProfessor())));
		lblOrientador.setFont(new Font("Tahoma", Font.BOLD, 11));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lbEmpresa, GroupLayout.PREFERRED_SIZE, 316, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lbInicio, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lbAluno, GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btNovaVisita, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btRemover, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnEditar, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lbtermino, GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
							.addContainerGap())))
				.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
					.addComponent(lblOrientador, GroupLayout.PREFERRED_SIZE, 434, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(346, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lbAluno, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnEditar, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
							.addComponent(btRemover, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
							.addComponent(btNovaVisita, GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lbEmpresa, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(lbInicio, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
							.addComponent(lbtermino, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)))
					.addGap(2)
					.addComponent(lblOrientador, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
		);
		panel.setLayout(gl_panel);

		pnArea.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.TRAILING)
						.addComponent(panel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
						.addComponent(pnArea, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE))
					.addGap(1))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnArea, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE))
		);

		contentPanel.setLayout(gl_contentPanel);
		btNovaVisita.addActionListener(new ouvinte());
		btRemover.addActionListener(new ouvinte());

		setLocationRelativeTo(null);
		setVisible(true);
	}
	private ArrayList<Visita> filtrarVisitas(ArrayList<Visita> todasVisitas) {
		ArrayList<Visita> visitasEstagio = new ArrayList<>();
		for (Visita visita : todasVisitas) {
			if(visita.getEstagio()==estagio.getId()) {
				visitasEstagio.add(visita);
			}
		}
		return visitasEstagio;
	}

	public void atualizarTabela() {
		JTVisitas = new JTable();
		JTVisitas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		filtrarVisitas(ManipularVisita.getInstace().listaTodos());

		JTVisitas.setModel(modelTabela = new TableModelVisita(filtrarVisitas(ManipularVisita.getInstace().listaTodos())));
		scrolVisita.setViewportView(JTVisitas);

		JTVisitas.getTableHeader().setReorderingAllowed(false);
		pnArea.setLayout(new BorderLayout(0, 0));
		
	}
	
	
	
	
	
	public class ouvinte implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==btNovaVisita) {
				new DialogCadastrarVisita(estagio);
				atualizarTabela();
			}if(e.getSource()==btnEditar) { 
				JOptionPane.showConfirmDialog(null, "Opcao ainda em desenolvimento");
			}if(e.getSource()==btRemover) { 
				Visita a = modelTabela.getVisita(JTVisitas.getSelectedRow());
				int op = JOptionPane.showConfirmDialog(null, "Realmente deseja remover esta visita?","Remover Visita",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
				if(op==0) {
					modelTabela.removerVisita(JTVisitas.getSelectedRow());
					ManipularVisita.getInstace().remover(a);					
				}
			}
			
		}
		
	}
}

package Impressao;

import java.io.File;

//
//Gerar PDF usa
//Add Build Path lib/itextpdf-5.4.1.jar
//

import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

// Para visualizar em tela apos gerar PDF
import java.awt.Desktop;

public class ListaEstagio {

	private Document document = new Document();
	private Desktop desktop  = java.awt.Desktop.getDesktop();

	JLabel label = new JLabel();
	
	public ListaEstagio() {

		document = new Document();
		

		try {

			PdfWriter.getInstance(document, new FileOutputStream("PDF_Aluno.pdf"));
			document.open();

			// adicionando um parágrafo no documento
			document.add(new Paragraph(cabecalho()));
			
			
		} catch (DocumentException de) {
			JOptionPane.showMessageDialog(null, "Erro no documento.");
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, "Error no arquivo.");
		}
		document.close();

		// Visualizar PDF com leitor padrão
		try {
			desktop.open(new File("planilha.xlsx"));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error na abertura do arquivo.");
		}
	}

	private String cabecalho() {

		label.setText("<html>primeira linha <br /> segunda linha</html>");
		
		
		return label.getText();
	}

}

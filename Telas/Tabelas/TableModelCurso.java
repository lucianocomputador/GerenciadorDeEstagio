package Tabelas;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entidades.Aluno;
import entidades.Curso;
import entidades.Orientador;


/** Classe responsavel por tratar a tabela Orientador
 * 
 * @author IgorCs 
 *
 */
@SuppressWarnings("serial")
public class TableModelCurso extends AbstractTableModel {

	private ArrayList<Curso> linhas;
	
	private String[] colunas = new String[] { "Nome"};
	private final static int nome = 0;

	
	public TableModelCurso(ArrayList<Curso> listaOrientador) {
		
		linhas = new ArrayList<Curso>(listaOrientador);
		
	}

	public int getRowCount() {
		return linhas.size();
	}

	public int getColumnCount() {
		return colunas.length;
	}

	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	}

	public Class<?> getColumnClass(int columnIndex) {

		switch (columnIndex) {

		case nome:
			return String.class;
		
			
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
		}

	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	

	public Object getValueAt(int rowIndex, int columnIndex) {

		Curso sala = linhas.get(rowIndex);

		switch (columnIndex) {
		case nome:
			return sala.getNome();
	
		
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");

		}
	}

	public Curso getCurso(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

	public void addAluno(Curso aluno) {
		linhas.add(aluno);

		int ultimoIndice = getRowCount();

		fireTableRowsInserted(ultimoIndice, ultimoIndice);
	}

	public void removerCurso(int indiceLinha) {

		linhas.remove(indiceLinha);
		fireTableRowsDeleted(indiceLinha, indiceLinha);
	}
}
package Tabelas;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;


import entidades.Estagio;


/** Classe responsavel por tratar a tabela Alcoar
 * @author IgorCs 
 *
 */
@SuppressWarnings("serial")
public class TableModelEstagio extends AbstractTableModel {

	private ArrayList<Estagio> linhas;
	
	private String[] colunas = new String[] { "Aluno", "Instituicao","Data Inicio","Data Fin","Processo inicial","Processo Final","Nota"};
	private final static int aluno = 0,instituicao = 1,dataInico = 2,dataFin = 3,processoInicial= 4,processoFinal=5,nota=6;

	
	public TableModelEstagio(ArrayList<Estagio> listaDeSalas) {
		linhas = new ArrayList<Estagio>(listaDeSalas);
	}

	public int getRowCount() {
		return linhas.size();
	}
//
	public int getColumnCount() {
		return colunas.length;
	}

	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	}

	public Class<?> getColumnClass(int columnIndex) {

		switch (columnIndex) {

		case aluno:
			return String.class;
		case instituicao:
			return String.class;
		case dataInico:
			return SimpleDateFormat.class;
		case dataFin:
			return SimpleDateFormat.class;
		case processoInicial:
			return String.class;
		case processoFinal:
			return String.class;
		case nota:
			return int.class;	
			
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
		}

	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	

	public Object getValueAt(int rowIndex, int columnIndex) {

		Estagio est = linhas.get(rowIndex);

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");		
		
		switch (columnIndex) {
		case aluno:
			return est.getAluno();
		case instituicao:
			return est.getInstituicao();
		case dataInico:
			return formato.format(est.getDataInicial());
		case dataFin:
			return formato.format(est.getDataFinal());
		case processoInicial:
			return est.getProtocoloInicial();
		case processoFinal:
			return est.getProtocoloFinal();
		case nota:
			return est.getNota();
			
			
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");

		}
	}

	public Estagio getEstagio(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

	public void addAluno(Estagio aluno) {
		linhas.add(aluno);

		int ultimoIndice = getRowCount();

		fireTableRowsInserted(ultimoIndice, ultimoIndice);
	}

	public void removerAluno(int indiceLinha) {

		linhas.remove(indiceLinha);
		fireTableRowsDeleted(indiceLinha, indiceLinha);
	}
}
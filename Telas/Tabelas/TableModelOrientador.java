package Tabelas;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import entidades.Aluno;
import entidades.Orientador;


/** Classe responsavel por tratar a tabela Orientador
 * 
 * @author IgorCs 
 *
 */
@SuppressWarnings("serial")
public class TableModelOrientador extends AbstractTableModel {

	private ArrayList<Orientador> linhas;
	
	private String[] colunas = new String[] { "Nome", "Matricula"};
	private final static int nome = 0,matricula = 1;

	
	public TableModelOrientador(ArrayList<Orientador> listaOrientador) {
		
		linhas = new ArrayList<Orientador>(listaOrientador);
		
	}

	public int getRowCount() {
		return linhas.size();
	}

	public int getColumnCount() {
		return colunas.length;
	}

	public String getColumnName(int columnIndex) {
		return colunas[columnIndex];
	}

	public Class<?> getColumnClass(int columnIndex) {

		switch (columnIndex) {

		case nome:
			return String.class;
		case matricula:
			return double.class;	
			
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");
		}

	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}
	

	public Object getValueAt(int rowIndex, int columnIndex) {

		Orientador sala =  linhas.get(rowIndex);

		switch (columnIndex) {
		case nome:
			return sala.getNome();
		case matricula:
			return sala.getMatricula();
		
		default:
			throw new IndexOutOfBoundsException("columnIndex out of bounds");

		}
	}

	public Orientador getOrientador(int indiceLinha) {
		return linhas.get(indiceLinha);
	}

	public void addOrientador(Orientador ori) {
		linhas.add(ori);

		int ultimoIndice = getRowCount();

		fireTableRowsInserted(ultimoIndice, ultimoIndice);
	}

	public void removerOrientador(int indiceLinha) {

		linhas.remove(indiceLinha);
		fireTableRowsDeleted(indiceLinha, indiceLinha);
	}
}
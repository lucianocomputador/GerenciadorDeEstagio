package validar;

import java.text.ParseException;

import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.text.JTextComponent;
import javax.swing.text.MaskFormatter;

public abstract class Validar {

	public static boolean isPreechido(JTextComponent comp) {
		if (comp.getText().equals(""))
			return true;
		else
			return false;
	}

	public static void setMascara(String maskFormat, JFormattedTextField jFormatedText, String validCar) {
		try {
			MaskFormatter mf = new MaskFormatter(maskFormat);
			mf.setValidCharacters(validCar);
			mf.setPlaceholderCharacter(' ');
			mf.install(jFormatedText);
		} catch (ParseException pe) {
			JOptionPane.showMessageDialog(null, "Erro na convers�o dos dados no campo!" + pe, "Erro!",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	public static double pegarNumeros(String num) {
		num = num.replaceAll("[^a-zZ-Z0-9 ]", "");

		double a = Double.valueOf(num);
		return a;

	}

	

}

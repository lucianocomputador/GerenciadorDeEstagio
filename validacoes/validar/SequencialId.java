package validar;

import java.util.ArrayList;


import entidades.Curso;
import entidades.Estagio;

public abstract class SequencialId {

	public static int sequencialIdEstagio(ArrayList<Estagio> lista) {
		int num = 0;
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).getId()==i) {
				num=i+1;
			}else {
				return num;
			}
		}
		return num;
	}
	
	public static int sequencialIdCurso(ArrayList<Curso> lista) {
		int num = 0;
		for(int i = 0;i<lista.size();i++) {
			if(lista.get(i).getId()==i) {
				num=i+1;
			}else {
				return num;
			}
		}
		return num;
	}
	
	
	
	
	
}
